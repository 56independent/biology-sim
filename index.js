THINGS_IN_THE_TANK = [] // TODO: I'm not sure if this is possible, but i'd lke to put this global ver in the tank... eh, who's going to need multiple tanks? LMAO nerds.

WIDTH = 1000
HEIGHT = 750
GOAL_FRAME_RATE = 25 // Lowballing to make it more cohesiveas more objects come in
MAX_SPEED = 1000

class tank {
    updateThings () {
        for (const thing of THINGS_IN_THE_TANK) {
            if (thing) {
                thing.update()
            }
        }

        // if (frameCount % (WIDTH*0.001) == 0){
        // }
    }

    constructor (){
        
        let renderer = new render;
        THINGS_IN_THE_TANK.push(new amoeba(Math.random()*WIDTH, Math.random()*HEIGHT, 1, 1, THINGS_IN_THE_TANK.length))
        THINGS_IN_THE_TANK.push(new algea())
        THINGS_IN_THE_TANK.push(new plants(THINGS_IN_THE_TANK[1].x))
        THINGS_IN_THE_TANK.push(new virus(Math.random()*WIDTH, Math.random()*HEIGHT, 0, 0, THINGS_IN_THE_TANK.length))
        
        this.draw = () => {renderer.draw(THINGS_IN_THE_TANK, 1, [1, 1])}
    }
}

let tankGuy;
let cnv

function setup() {
    tankGuy = new tank();
    cnv = createCanvas(WIDTH, HEIGHT); // Adjust canvas size as needed
    
    frameRate(GOAL_FRAME_RATE); 
    for (let i = 0; i < 10; i++) {
        THINGS_IN_THE_TANK[0].splitReproduce()
     } // TODO: Explain this code and how it makes 50 ameoba copies for virus protection

    //cnv.mousePressed = tankGuy.handlePresses(mouseX, mouseY)
}

function draw() {
    cnv.background(59, 123, 206)
    tankGuy.updateThings()
    tankGuy.draw()
}

