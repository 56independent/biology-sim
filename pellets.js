class pellet extends tankObject {
    update() {
        if (!this.clicked && !this.notPelletMotion) {
            this.speedx = ((0.5 - Math.random())*0.5)*50
            this.speedy = this.gravity
        }
        
        super.update();
    }

    constructor(x, y, speedx, speedy, myIndex) {
        super(x, y, speedx, speedy, myIndex)
        this.gravity = 20;
    }
}