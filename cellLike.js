class cellLike extends tankObject{
    update () {
        if (this.ageInSeconds > 0 && frameCount%GOAL_FRAME_RATE == 0) {
            this.energy -= this.energyLossRate
            this.health -= this.healthLossRate
            this.respiration -= this.respirationLossRate
        }

        if (this.energy <= 0
            || this.health <= 0
            || this.respiration <= 0) {
                console.log("A cell just died!", THINGS_IN_THE_TANK[this.index])
                
                // Time to "explode" out any virii in the cell
                for (const thing of getDistances("virus", this, THINGS_IN_THE_TANK)) {
                    if (thing[0] < 10) {
                        thing[3].copies = 0
                        thing[3].infectingForeignCell = false // We can now infect other celli
                        thing[3].freeMotionFromInfectedCell = true
                        thing[3].speedx = (Math.random()-0.5)*50
                        thing[3].speedy = (Math.random()-0.5)*50

                    }
                }

                THINGS_IN_THE_TANK[this.index] = null
            }

        super.update()
    }

    draw (colour) {
        let energyas1 = this.energy/200
        let healthas1 = this.health/100
        let respirationas1 = this.respiration/100

        let colourMultiplier = 1-energyas1

        strokeWeight((healthas1)*5)
        fill(
            (colour[0]*colourMultiplier)/respirationas1,
            (colour[1]*colourMultiplier)/respirationas1,
            (colour[2]*colourMultiplier)*respirationas1)
    }

    generator() {
        let list = []
        let number = Math.random()*45+5
        for (let i=0; i < 25; i++) {
            while (list.includes(number)){
                number = Math.random()*45+5
            }
            list.push(number)
        }
        return list
    }

    getRandomSubarray(arr, size) {
        return [] // TODO: fix this function
        var shuffled = arr.slice(0), i = arr.length, temp, index;
        while (i--) {
            index = Math.floor((i + 1) * Math.random());
            temp = shuffled[index];
            shuffled[index] = shuffled[i];
            shuffled[i] = temp;
        }
        return shuffled.slice(0, size);
    }

    constructor (x, y, speedx, speedy, myIndex, parentAntibodies=null, antibodyCount=10) {
        super(x, y, speedx, speedy, myIndex)
        
        // In percentage points
        this.energy = 100
        this.health = 100
        this.respiration = 100

        // In units per second
        this.energyLossRate = 3
        this.healthLossRate = 0.1 // Aging
        this.respirationLossRate = 0.2 // TODO: let cells breathe!

        this.reproductionRate = 30

        /*
        * There are 50 possible antibodies, each number-indexed from 1 to 50.
        * Organisms may only choose 30 of these 50 for their operations
        * All organisms share the first 5 antibodies
        * All cells may only activate 10 antibodies
        * Species are determined by a random selection of 30 antibodies
        * Ameobas only have 5 active antibodies, which are permament. The selections are random. 
        * Viruses have one active antibody selected at creation
        * The active antibodies are printed using text. A better method is yet to be found.
        */

        this.speciesAntibodies = parentAntibodies || [1, 2, 3, 4, 5].push(this.generator())
        this.activeAntibodies = this.getRandomSubarray(this.speciesAntibodies, antibodyCount)
        
    }
}