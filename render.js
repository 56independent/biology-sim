class render {
    adjustForViewport([viewportx, viewporty], [originalx, originaly], viewportSizeMultiplier) { // TODO: use size to verify wether something should be drawn at all
        // Viewport co-ords specify where we are in the canvas, according to the global co-ord system.
        // The original co-ords specify where the object is according to the global system
        // Our goal here is to adjust it to the viewport's co-ordinate system

        // To do this, we find how far the object is from the viewport

        let distancex = originalx-viewportx
        let distancey = originaly-viewporty

        // Now we reign it in with the multiplier

        let newDistancex = distancex*viewportSizeMultiplier
        let newDistancey = distancey*viewportSizeMultiplier

        // And let's give it back:
        return [newDistancex, newDistancey]

    }

    draw (tankThings, zoom, [viewportx, viewporty]) { // With Tankthings being the THINGS_IN_TANK array, containing many objects.()
        clear()
        for (const thing of tankThings) {
            if (thing) {
                let newCoords = this.adjustForViewport([viewportx, viewporty], [thing.x, thing.y], zoom)
                translate(newCoords[0], newCoords[1]);
                thing.draw()

                // if(thing.x = 500){
                //     console.log(thing)
                // }

                // Reset everything
                resetMatrix()
                fill("white")
                stroke("black")
                strokeWeight(2)
            }
        }
    }
}