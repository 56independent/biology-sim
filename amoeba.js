class amoeba extends singleCell {
    splitReproduce() {
        let newAmoeba = new amoeba(this.x+Math.random()*50, this.y, Math.random()*50, 0, THINGS_IN_THE_TANK.length, this.speciesAntibodies)
        THINGS_IN_THE_TANK.push(newAmoeba)

        newAmoeba.energy = 70
        this.energy = 90
    }

    update() {
        // * if `this.goal` is false:
        //     * Find the nearest food group
        //     * Find its co-ords and relative distance
        //     * Set this.target to that
        //     * Set `this.goal`
        // * else:
        //     * Continue moving to the goal by keeping the speed ok; it's porpotional to energy.
        // if (!this.x && !this.y) {
        //     this.x = WIDTH*0.5
        //     this.y = HEIGHT*0.5
        // }


        let touchingFood = false
        let target = THINGS_IN_THE_TANK[this.targetIndex];

        for (const foodle of getDistances("food", this, THINGS_IN_THE_TANK)) {
            if (foodle[0] < 50){
                THINGS_IN_THE_TANK[foodle[3].index] = null
                this.energy += 25
                this.goal = false
            }
        }

        if (touchingFood) {
            goal = false
            THINGS_IN_THE_TANK[this.targetIndex] = null
            energy += 100
        }

        if (this.energy >= 150) {
            this.goal = false
            this.speedx = 0
            this.speedy = 0
        }

        // if (this.goal && target) {
            // Our target is in target.x and target.y. We want to, porportional to 10, do things.
            // We want to go directly to the target. This can be done by finding its distance from us.

            approachNearest("food", this, THINGS_IN_THE_TANK, 30)
        // } else {
        //     this.speedx = 0
        //     this.speedy = 0

        //     let distances = getDistances("food", this, THINGS_IN_THE_TANK)
        //     let goodCandidates = distances //[] // TODO: Add cluster hadnling logic.

        //     // for (let i=0; goodCandidates.length > 1; i++) {
        //     //     goodCandidates = findPointAreas(i, this.x, this.y, distances)
        //     //     console.log(goodCandidates)
        //     // }

        //     if (goodCandidates.length > 1) {
        //         //console.log(goodCandidates[1][3])
        //         this.targetIndex = goodCandidates[0][3].index
        //         this.goal = true

        //         //console.log(this.target)
        //     }
        // }

        if (this.energy >= 140) {
            this.splitReproduce()
        }

        super.update()
    }

    draw() {
        // TODO: Add proper Amoeba logic

        let colour = [213, 130, 213]
        super.draw(colour)

        beginShape()
        vertex(30, 0)
        vertex(0, 30)
        vertex(-30, 0)
        vertex(0, -30)
        endShape(CLOSE)
    }

    reproduce() {

    }

    constructor(x, y, speedx, speedy, myIndex, speciesAntibodies) {
        super(x, y, speedx, speedy, myIndex, speciesAntibodies, 5)

        this.target = []
        this.goal = false
        this.infected = false
    }
}
