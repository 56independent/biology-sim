class tankObject {
    update() {
        if (this.x > WIDTH+100 || this.y > HEIGHT+100 || this.x < 0-100 || this.y < 0-100) {
            THINGS_IN_THE_TANK[this.index] = null
        }
        this.age = frameCount-this.epoch
        this.ageInSeconds = this.age/GOAL_FRAME_RATE

        if (mouseIsPressed == true) {
            if (this.clicked || (this.x+10 > mouseX && this.x-10 < mouseX && this.y+10 > mouseY && this.y-10 < mouseY)){
                this.clicked = true
                
                this.x = mouseX
                this.y = mouseY

                this.speedx = movedX
                this.speedy = movedY
                return true;
            }
        }
        this.clicked = false
        
        if (!this.still) {
            this.speedx = Math.min(MAX_SPEED, this.speedx);
            this.speedy = Math.min(MAX_SPEED, this.speedy);
        
            this.x += (this.speedx / GOAL_FRAME_RATE);
            this.y += (this.speedy / GOAL_FRAME_RATE);
        }

        // // Adjust speedy for water de-celeration
        // this.speedy = (this.speedy > 2) ? this.speedy - 2 * frameRateRatio : 
        //             (this.speedy < -2) ? this.speedy + 2 * frameRateRatio : 
        //             this.speedy * 0.5 * frameRateRatio;
    }

    constructor(x, y, speedx, speedy, myIndex) {
        // Speed should be in coordinates per second
        // To convert between CPS and CPF (co-ordinates per frame), we use the equation Co-ordinates/second = co-ordinates/(frames elapsed/frame_rate)

        this.x = x;
        this.y = y;
        this.speedx = speedx;
        this.speedy = speedy;
        this.clicked = false

        this.epoch = frameCount
        this.index = myIndex
        this.targeted = false
    }
}
