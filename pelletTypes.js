class food extends pellet {
    update() {
        // let framesBeforeDeath = 30*GOAL_FRAME_RATE
        // if (this.age > framesBeforeDeath) {
        //     //console.log(this.index, THINGS_IN_THE_TANK)
        //     let wasteParticle = new waste(this.x, this.y, this.speedx, this.speedy, THINGS_IN_THE_TANK.length)
        //     THINGS_IN_THE_TANK.push(wasteParticle)
        //     THINGS_IN_THE_TANK[this.index] = null
        // }
        
        super.update()
    }
    
    draw () {
        fill("green")
        circle(0, 0, 20);
    }
}

class waste extends pellet {
    draw () {
        let transparencyMultiplier = 0.25
        if (100-(this.ageInSeconds*transparencyMultiplier) > 0) {
            fill(100, 50, 50, 100-(this.age*transparencyMultiplier))
            stroke(0, 0, 0, 100-(this.age*transparencyMultiplier) )
            circle(0, 0, 20);
        } else {
            THINGS_IN_THE_TANK[this.index] = null
        }
    }
}

class virus extends pellet {
    infect(victim){
        //console.log(victim)
        this.x = victim.x
        this.y = victim.y
        this.victim = victim
        this.freeMotionFromInfectedCell = false
        this.infectingForeignCell = true
        this.copies = (Math.random()*25)+25

        victim.infected = true
    }
    
    update(){
        this.gravity = 0
        
        if (this.infectingForeignCell && haveXSecondsElapsed(0.25)) { // We want to reproduce 4 times a second
            
            if (this.copies > 0 && this.victim) {
                THINGS_IN_THE_TANK.push(new virus(this.x, this.y, 0, 0, THINGS_IN_THE_TANK.length, this.victim))
                this.copies--
                this.victim.health -= 4

                if (this.victim.health <= 50) {
                    this.victim.speedx = 0
                    this.victim.speedy = 0
                }
            } else if (!this.victim) {
                this.speedx = this.speedx || (Math.random()-0.5)*50
                this.speedy = this.speedy || (Math.random()-0.5)*50
            }


            super.update()
            return
        } else if (!this.infectingForeignCell) { // Virii should stay loyal to their homes.
            let collisionHappened = collisionHandler(THINGS_IN_THE_TANK, "amoeba", this, 50)
            
            if (!collisionHappened[0] || collisionHappened[1].infected == true){
                approachNearest("amoeba", this, THINGS_IN_THE_TANK, 15, true)
            } else {
                this.infect(collisionHappened[1])
            }
        } else {
            if (!this.freeMotionFromInfectedCell) {
                this.x = this.victim.x
                this.y = this.victim.y
            }
        }

        super.update()

    }
    
    draw () {
        fill("blue")
        circle(0, 0, 20);
    }

    constructor(x, y, speedx, speedy, myIndex, victim=null){
        super(x, y, speedx, speedy, myIndex)

        this.notPelletMotion = true
        this.victim = victim
        if (victim) {
            this.infectingForeignCell = true
            this.copies = 0
            this.freeMotionFromInfectedCell = false
        } else this.infectingForeignCell = false
    }
}


class bodyThing extends pellet {
    draw () {
        circle(0, 0, 10)
    }
}

class whiteOmmuniser extends bodyThing {}
class glucose extends bodyThing {}
class bubbles extends bodyThing {
    update() {
        this.gravity = -20
        super.update()
    }
    
    draw () {
        fill("white")
        stroke("black")
        circle(0, 0, 20)
    }
}
class oxygen extends bodyThing {}
class dryStemCell extends bodyThing {}
class singlet extends bodyThing {}

class hormone extends bodyThing {}
class antibodiesHormone extends hormone {}
class cellMover extends hormone {}
class stemChange extends hormone {}
class reproduce extends hormone {}
