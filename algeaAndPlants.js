class lineOrganism extends singleCell {
    update () {
        super.update()
    }

    draw() {
        super.draw([200, 40, 40])
        rect(0, 0, 30, 10)
    }

    constructor (y, x=NaN) {
        const permaX = x || Math.random()*WIDTH
        super(permaX, y, 0, 0, THINGS_IN_THE_TANK.index)

        this.still = true
    }
}

class algea extends lineOrganism {
    reproduce() {
        let distances = getDistances("algea", this, THINGS_IN_THE_TANK)

        let x = 0

        console.log("reproducing")

        if (distances.length != 0) {
            let lastCell = distances[distances.length-1]
            x = lastCell.x+40
        } else {
            x = 10
        }

        THINGS_IN_THE_TANK.push(new algea(x, true))
        THINGS_IN_THE_TANK[2].reproduce()

        return
    }

    feed() {
        THINGS_IN_THE_TANK.push(new food(this.x+(30/2), this.y+10, 0, 0, THINGS_IN_THE_TANK.length))
    }

    update () {
        if (haveXSecondsElapsed(5)){ // Generate food every three seconds.
            this.feed()
        }
        if (haveXSecondsElapsed(3)){
            if (!this.reproduceLock) this.reproduce() // Prevent recursive reproduction; https://chat.openai.com/share/7fea737d-1f61-4ad8-823a-5ed9c1265468
        }

        this.speedy = 0
        this.speedx = 0
        super.update()
        this.speedy = 0
        this.speedx = 0
    }

    constructor (x=NaN, reproduce=false) {
        super(30, x)

        this.reproduceLock = reproduce
        this.energyLossRate = 0 // We're photosynthesising, baby!
    }
}

class plants extends lineOrganism {
    reproduce() {
        let distances = getDistances("plants", this, THINGS_IN_THE_TANK)

        let x = 0

        console.log("reproducing")

        if (distances.length != 0) {
            let lastCell = distances[distances.length-1]
            x = lastCell.x+40
        } else {
            x = 10
        }

        THINGS_IN_THE_TANK.push(new plants(x, true))
    }
    update () {
        // Turn foods into bubbles
        let touchingFood = false

        for (const foodle of getDistances("food", this, THINGS_IN_THE_TANK)) {
            if (foodle[0] < 50){
                let object = foodle[3]
                let x = object.x
                THINGS_IN_THE_TANK[object.index] = new bubbles(object.x, object.y, 0, 0, object.index)
                for (let i in 5) {
                    THINGS_IN_THE_TANK.push(new bubbles(x, this.y, 0, 0, object.index))
                }
            }
        }

        if (touchingFood) {
            goal = false
            THINGS_IN_THE_TANK[this.targetIndex] = null
            energy += 100
        }
    }

    constructor (x) {
        super(HEIGHT-20, x)
    }
}