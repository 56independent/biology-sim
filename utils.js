// I've heard having a utils file is bad practice, but IDRC because it's a helpful box to put things in

function calculateDistance(x1, y1, x2, y2) {
    const deltaX = x2 - x1;
    const deltaY = y2 - y1;
    
    // Using the Pythagorean theorem to find the straight-line distance
    const distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);
    
    return distance;
}

function getDistances(itemTypes, ourHandle, items){
    let ourx = ourHandle.x
    let oury = ourHandle.y
    let distances = []
    
    //console.log(itemTypes, ourHandle, items, ourx)
    for (const item of items) {
        if(item && item.constructor.name == itemTypes && item != ourHandle) {
            let distance = calculateDistance(ourx, oury, item.x, item.y)
            distances.push([distance, [ourx-item.x, oury-item.y], [item.x, item.y], item])
        }
    }
    
    //console.log(distances)
    
    return distances.sort((a, b) => a[1] - b[1])
}

function findPointAreas(radius, ourx, oury, points) {
    // Given a 2d matrix of distances (index 1) and co-ordinates (index 2), find clusters of points in good areas
    
    let candidates = []
    
    for (point in points) {
        if (point[1] <= radius){
            console.log(point, candidates)
            candidates.push(point) // TODO: Find clusters
        }
    }
    
    return candidates
}

function calculateSpeedNeededAlongThisAxis(distance, speed, proportion) {
    const maxSpeed = proportion;
    
    // Ensure speed is not zero
    const nonZeroSpeed = speed !== 0 ? speed : 1;
    
    // Use the absolute value of distance
    const absDistance = Math.abs(distance);
    
    const distanceSquared = Math.pow(absDistance * nonZeroSpeed, 2);
    const speedResult = Math.min(Math.sqrt(distanceSquared), maxSpeed);
    
    return speedResult;
}

function collisionHandler(objects, type, us, distance){
    let result = false
    let collider
    
    for (const foodle of getDistances(type, us, objects)) {
        if (foodle[0] < distance){
            result = true
            collider = foodle[3]
        }
    }
    
    return [result, collider]
}

function approachNearest (type, us, them, speedLimit, energyConserveDisable=false) {
    let distances = getDistances(type, us, them)
    let goodCandidates = distances //[] // TODO: Add cluster hadnling logic.
    
    // for (let i=0; goodCandidates.length > 1; i++) {
    //     goodCandidates = findPointAreas(i, this.x, this.y, distances)
    //     console.log(goodCandidates)
    // }

    let foundGoodTarget = false
    
    // if (goodCandidates.length >= 1) {
        for (const targetInfo of goodCandidates) {
            let target = targetInfo[3]

            if (!target.targeted || target.targeted == us) {
                //console.log("foundTarget", target)
                // Our target is in target.x and target.y. We want to, porportional to 10, do things.
                // We want to go directly to the target. This can be done by finding its distance from us.
                
                // Calculate the distance
                let distancex = target.x - us.x;
                let distancey = target.y - us.y;
                let distance = Math.sqrt(distancex * distancex + distancey * distancey);
                
                //console.log(this.speedx)
                
                // Normalize the speed vector
                us.speedx = distancex / distance;
                us.speedy = distancey / distance;
                
                // Scale the speed vector to ensure it never exceeds 10
                us.speedx *= speedLimit;
                us.speedy *= speedLimit;

                foundGoodTarget = true
                target.targeted = us
                break
            }
        }
    // } 
    if (!energyConserveDisable && !foundGoodTarget) {
        // Conserve energy and stop
        us.speedx = 0
        us.speedy = 0
    }
    
}

function haveXSecondsElapsed(x) {
    return frameCount%(GOAL_FRAME_RATE*x) == 0
}

function printMessage(message) {
    messager = new messagingHandler(5)
    THINGS_IN_THE_TANK.push(messager)
    messager.giveMessage(message)
}

class messagingHandler {
    giveMessage(message) {
        this.drawObjects.push(() => {
            textSize(100);
            text(message, WIDTH*0.5, HEIGHT*0.5);
        })
    }

    draw() {
        for (let drawObject of drawObjects){
            drawObject()
        }
    }

    update() {
        if (frameCount = this.removeTime) {
            THINGS_IN_THE_TANK[this.myIndex] = null
        }
    }

    constructor (keepTime) {
        this.epoch = frameCount
        this.removeTime = frameCount*(GOAL_FRAME_RATE*keepTime)

        this.myIndex = THINGS_IN_THE_TANK.length
        this.drawObjects = []
    }
}
