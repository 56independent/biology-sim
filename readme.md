# Using
This code is a biology simulator. By the end of the project, we plan to have this functionality:

* Click and dragging to move things
* Right-clicking to "hurt" things; make cells lose their health or turn things into waste
* Keybinds for things
    * `a` to spawn an amoeba
    * `v` to spawn a virus
    * `tab` to decrease speed, `del` to increase
    * `b` to spawn a bacterium
    * `i` to spawn an insect
    * `a` to spawn an animal with a random organ map
* Good animation
* Collision handling
* A tidy codebase based off a class hierachy

## Basics
The simulation takes place a tank of water, within which, creatures and various other things float around. 

Food is dropped into the tank at a constant rate, which eventually becomes waste as it rots away.

Air bubbles come from the bottom and float up. These are turned into oxygen by some cells.

Each cell has energy, breath, and health. These are the so-called "life forces". If any of these hit zero, the cell dies and turns into waste particles. Energy is represented by the cell innards's saturations, the breath by the blue tint of the cell, which increases as it loses breath, and health by the thickness of the cell wall.

Each living creature has two main groups of antibodies; the "possible antibodies" and the "active antibodies". There are about 50 antibody types in the game, with 30 available for each organism, identified by a three-digit number. 5 antibodies are deemed universal. "active antibodies" are antibodies that the cell will interact with, and there is a hard limit of 10 for all organisms. Bigger organisms can dynamically change their active antibodies based off mixtures of their possible antibodies.

## Creatures
All creatures are formed of cells or some form of them. Cells can die in various ways, age being one of the main ones. As well as this, if any of their life forces reach 0, they also die. Infections can also destroy the cells.

### Single-celled
#### Virii
Blue dots moving laterally with the natural "turbulence" in the water; will be "pulled" towards organisms, reproduce by displacing all other functions of the cell, and eventually kill them through energy expenditure. Virii turn to waste after a while. 

#### Amoebas
Organisms which manouver to food, eat it, and rest for a little while. If they get enough energy, they reproduce. They have a for-life set of antibodies, set at the reproduction stage. These are 5 active antibodies. In reproduction, amoebas store 50% more life statistics then 100% and then split it 90%/60%, whith the smaller porportion going to a "child" amoeba with a different active antibody set. 

Amoebas have "species" determined by their 30 antibodies possible antibodies of which 5 are chosen during reproduction to be permanently active.

#### Bacteria
Bacteria are parasites, in a way. They can exist on their own but are inneficent at handling food. Instead, they are better suited for glucose consumption. As such, they generally seek out a multicellular organism to use its glucose, which is far more effective. However, given its higher concentration, toxins are released.

They, like amoebas, have 5 permanently-active antibodies. Since body particles are antigen-encoded, bacteria has to find organisms with the correct antibodies.

#### Algea
Algea sit at the top of the board and provide food from the sun. This feeds all the animals.

#### Plants
Plants sit at the bottom and convert food into air, which floats up.

### Multicellular
Multicellular organisms are formed of a diverse group of cells doing different roles. For in-cell communication, the different cells have "body particles". All body particles have a single active antigen which binds to active antibodies of the same type.

Glucose is a blue particle which carries energy. These are highly energy-dense and can bring a cell to 100% energy with just 50% of their capacity.

Oxygen is a small white particle which carries respiration energy. It is not a bubble but instead a representation of a high oxygen content. As such, they simply float around and use brownian motion to go around the cell. If enough oxygen is inside a organism, all its cells will have 100% respiration.

White immunisers are responsible for taking care of cell's health. They can see active antibodies and take decisions with them. The active antibodies of a cell allows them to choose what antigens they can take in, making this important for life. They seem like cells but only really have one survival force; the amount of uses they have left.

Toxins are formed by any cell using energy and will damage the health of any cell it collides with. They must be removed. They are neutralised in tank water to normal waste.

#### Insects
Insects are multi-cellular "cell bags", formed of a circular group of cells.

At the edges of the cell bag, movers, food-eaters, gates, and respirers all sit. These are responsible for taking resources from the outside environments. The outside wall of the cell bag can actually move particles around to be consumed. Movers simple provide force for movement. They co-ordinate to go to nearby food groups and collide, meaning it spreads around the cell. Food-eaters take food and convert it into blue glucose. Respirers take bubbles and keep oxygen levels inside the organism high using it. Finally, the gate controls what substances come in and out of the organism using a "vacuuming" process; toxins can be sucked out and the occasional organism innard in the tank can be sucked in. As well, the gate helps control water pressure between the two sides of the organism without causing chemical inbalances.

Inside the organism, there are various other types of cell. Mainly, there's the hormone repeater, stem cell, and "backup surface cells". The hormone repeater takes a hormone and repeats it 10 times. Backup surface cells sit there in a dormant, "naïve" mode, in which they are on standby to fill the wall as it expands or a cell dies. Finally, we have stem cells. These simply sit on standby unless they recieve a hormone specifying what cell to reproduce into and how many.

The body particles "squeeze" between the cells in the cell bag and find places they belong.

Stem cells are probably the most vital part of the organism. They help replace cells that die due to aging. If stem cells die, the entire organism will be killed eventully.

The death of an insect is quite an intresting thing to watch; if the wall of the bag develops a hole that isn't filled, the innards slowly empty out into the tank where they eventually lose the resources they need. Innards are quite sensitive and the water inside the tank is often at a danagerou Ph level which will deplete the health of the inner cell to 0. Body particles aren't very sensitive and will just sink to the bottom. As well as this, the inside of the cell might leak out its internal fluids as they're replaced with the tank fluids. This can truly kill a cell's insides, leaving simply the outer wall, which eventually goes old and dies.

### Animals
Animals are the most complex organisms in the game. They are formed of an intricate system of organs and vessels. They have all the insect cell types plus some more.

Every animal has a so-called "organ map" definining how the organs are arranged. See this example:

```
organs = [
    [null, "mover", "mover", null]
    ["mover", "food-eater", "food-eater", "mover"],
    ["food-eater", "stem cell", "respirer", "food-eater"],
    ["mover", "fat bank", "hormone repeater", "mover"],
    ["mover", "gate", "gate", "mover"]
    [null, "respirer", "respirer", null]
]
```

Each organ type contains a cell group of a single type which is extremely efficent and capable of production at extremely high rates.

Between each organ is a vessel arranged in a bidirectional network resembling a spider's web. At each junction are "heart" cells which keep the speed in the veins high and control the direction the things inside go. This vessel system provides high-speed body particle transport allowing the whole animal to share resources with itself.

There are a few new types of cell; the vessel cell, heart cell, and fat cell. The vessel cell is simply a long bidirectional conduit to contain the body particles flying around. The heart takes some glucose to keep speeds up and pressures high. The fat cell stores extra glucose as energy increases.

Boundary organs have a "sereprent" style of building which keeps surface area high. Their "vacuuming" force is extremely high and can pull in food from long distances.

When an animal dies of age, it will "explode" in a slow manner. Each of the organs will lose their connective tissues and the animal will slowly begin seperating with the turbulence of the tank. The vessels may not be connected to a central heart or at least a working heart, but they will still leech out the produced body tissues. As the inner cells die due to the tank water and the outer cells age away, the animal slowly gets demolished. This should leave a massive amount of body particles in the tank water, floating around.

Reproduction is also quite complex. The first thing that happens is that a hormone telling the animal to reproduce is sent. This can only happen with more then 10 cells per organ in the animal. The stem cell organs go into overdrive as they produce "dry stem cells" which are guided by the hearts into a predetermined location for the child animal to grow. The dry stem cells take on water and diversify themselves into the correct type This location is nestled in between two boundary organs, and as the fetus grows and pushes the boundary cells away. To fill the gap, "wall cells", dry, long husks of cells grow. These keep tank water out and protect the fetus.

As soon as there are 6 cells per organ in the fetus, the fetus is pushed out, breaking past the wall cells. As it leaves, the boundary cells "flatten" to fill the space left behind.

# Codebase
The codebase uses vanilla JS with p5.js as its framework. p5.js.

