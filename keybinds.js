function keyPressed() {
    // * `a` to spawn an amoeba
    // * `b` to spawn a bacterium
    // * `i` to spawn an insect
    // * `a` to spawn an animal with a random organ map

    switch (keyCode) {
        case TAB:
            frameRate(GOAL_FRAME_RATE)
            GOAL_FRAME_RATE = GOAL_FRAME_RATE*2
            printMessage("Simulation rate halved")
            break
        case DELETE:
            frameRate(GOAL_FRAME_RATE)
            GOAL_FRAME_RATE = GOAL_FRAME_RATE/2
            printMessage("Simulation rate doubled")
            break
    }
}

function keyTyped() {
    console.log(key)

    switch (key) {
        case "a":
            THINGS_IN_THE_TANK.push(new amoeba(Math.random()*WIDTH, Math.random()*HEIGHT, 1, 1, THINGS_IN_THE_TANK.length))
            printMessage("New amoeba added")
            break
        case "v":
            THINGS_IN_THE_TANK.push(new virus(Math.random()*WIDTH, Math.random()*HEIGHT, 0, 0, THINGS_IN_THE_TANK.length))
            printMessage("New virus added")
            break
        case "f":
            THINGS_IN_THE_TANK.push(new food(Math.random()*WIDTH, 30, 0, 0, THINGS_IN_THE_TANK.length))
            break
    }
}
